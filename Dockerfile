FROM ubuntu

RUN apt-get update \
&& apt-get install -y \
    wget \
&& rm -rf /var/lib/apt/lists/*
RUN wget --output-document tiobj2bin-linux.tar.gz https://gitlab.com/onplick/ti-obj2bin-linux/raw/master/tiobj2bin-linux.tar.gz \
&& tar -xzf tiobj2bin-linux.tar.gz \
&& rm -f tiobj2bin-linux.tar.gz
